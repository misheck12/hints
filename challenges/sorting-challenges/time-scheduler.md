<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In an algorithm design, there is no one "silver bullet" that is a cure for all computation problems. Different problems require the use of different kinds of techniques. A good programmer uses any or all of these techniques based on the type of problem. Some commonly used techniques are:<br>
&emsp;\- Divide-and-conquer<br>
&emsp;\- Randomized algorithms<br>
&emsp;\- **Greedy algorithms**<br>
&emsp;\- Dynamic programming

---

<br>

The problem here is to find the largest possible set of non-overlapping events from the given [start, end] pairs of events.<br>
<img src="images/sorting-challenges/time-scheduler/time-scheduler-1.png"  width="800"><br>

The brute-force solution finds all possible subsets of events, excludes those with overlapping events, then selects the largest one as its result.<br>

&emsp;\- It will take O(n^2) to find all of the subsets. This is pretty bad on its own.<br>
&emsp;\- On the other hand, the comparison will take O(n+m); so the resulting overall complexity is O(n^2)—as we noted above, this is pretty bad.



---

<br>

In this challenge, we will follow the **greedy algorithm** approach to arrive at the optiomal solution with **O(n * lg(n))** time complexity.

A greedy algorithm, as the name suggests, always makes the choice is the best at that moment. That is, it makes a locally optimal choice in the hope that it will lead to a globally optimal solution.

The toughest part of defining greedy algorithms is finding the substructure for making the best choice. We need to consider all information available.

In this problem, the greedy pattern is making a choice on earlier ending events. We should ensure that the later events are less affected by the chosen event.

<img src="images/sorting-challenges/time-scheduler/time-scheduler-2.png"  width="800"><br>

<b>`Hint:`&nbsp;Sort the events by their end time in ascending order.<br>
<b>`Hint:`&nbsp;Select the first event; this event must be in the resulting set.<br>
<b>`Hint:`&nbsp;Perform a forward iteration to traverse each remaining events. Then, greedily choose the next event that does not overlap with the previously selected event.<br>
<b>`Hint:`&nbsp;To check if the events are overlapping, confirm that the end time of the previously selected event must be less than the start time of the current event.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to convert raw data into information</summary><br>

There is only an array given from the parameters. Working with raw data can complicate the solution. Before we implement the solution directly, we need to digest the information from the raw data.

As we discussed in the previous hints, we need to sort the events array by the end time of the events. We need a events array so that we can sort them easily.

```ruby
def time_scheduler(array)
  result = []
  # array = [1, 5, 2, 4] => event1 = [1, 5] event2 = [2, 4]
  events = (0...array.length).step(2).collect { |index| [array[index], array[index+1]] }
  # events = [[1, 2], [3, 4]]
  events.sort_by! { |event| event[1] }
  # events = [[3, 4], [1, 2]] after sort
end
```

<b>`Hint:`&nbsp;Push the first event into a result array.<br>
<b>`Hint:`&nbsp;Traverse all events and greedily choose the non-overlapping events.<br>
<b>`Hint:`&nbsp;At each step, check if the previously selected event overlaps with the current event. If so, exclude it. Otherwise, push it to the result array.<br>
  
</details>
<!-- Hint -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
def time_scheduler(array)
  events = (0...array.length).step(2).collect { |index| [array[index], array[index+1]] }
  events.sort_by! { |event| event[1] }
  result = [events[0]]
  for i in 1...events.length
    result << events[i] unless overlap?(result[-1], events[i])
  end

  result
end

def overlap?(event1, event2)
  event1[1] > event2[0]
end
```
  
</details>
<!-- Hint -->
